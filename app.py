from flask import *
import os
import pymongo
from bson.objectid import ObjectId
import datetime

DB_NAME = 'heroku_f1c44c95'  
DB_HOST = 'ds135456.mlab.com'
DB_PORT = 35456
DB_USER = 'admin' 
DB_PASS = 'password123'

myclient = pymongo.MongoClient(DB_HOST, DB_PORT)
mydb = myclient[DB_NAME]
mydb.authenticate(DB_USER, DB_PASS)
myexpenses = mydb['expenses']
mymoney = mydb['money']
myusers = mydb['users']
myincome = mydb['income']

app = Flask(__name__)
app.secret_key = 'budgeter-secret-key'

@app.context_processor
def override_url_for():
    return dict(url_for=dated_url_for)

def dated_url_for(endpoint, **values):
    if endpoint == 'static':
        filename = values.get('filename', None)
        if filename:
            file_path = os.path.join(app.root_path,
                                    endpoint, filename)
            values['q'] = int(os.stat(file_path).st_mtime)
    return url_for(endpoint, **values)

##
# PAGES
##

@app.route('/')
def home():
    if(check_if_session_is_active()):
        currentMoney = '{0:.2f}'.format(mymoney.find_one()['money'])
        expenses = myexpenses.find().sort('timestamp', -1)
        incomes = myincome.find().sort('timestamp', -1).limit(3)
        return render_template(
            'home.html',
            currentMoney=currentMoney,
            expenses=expenses,
            incomes=incomes,
            commarize=commarize
        )
    return render_template('landing.html')

@app.route('/login', methods=['POST', 'GET'])
def login():
    if(request.method == 'POST'):
        username = request.form['username']
        password = request.form['password']
        query = {
            'username': username,
            'password': password
        }
        try:
            search = list(myusers.find_one(query))
            session['active'] = True
            custom_flash('You have successfully logged in.')
        except TypeError:
            custom_flash('Invalid credentials.')
    return redirect(url_for('home'))

@app.route('/logout')
def logout():
    if(check_if_session_is_active()):
        session.pop('active', None)
        custom_flash('You have successfully logged out.')
    return redirect(url_for('home'))

@app.route('/expenses/add', methods=['POST', 'GET'])
def add_expense():
    if(request.method == 'POST'):
        if(check_if_session_is_active()):
            amount = '{0:.2f}'.format(float(request.form['amount']))
            expense = {
                'title': request.form['title'],
                'amount': amount,
                'category': request.form['category'],
                'timestamp': f'{current_time()}'
            }
            amount = float(amount)
            myexpenses.insert_one(expense)
            current_money = mymoney.find_one()['money']
            new_money = current_money - amount
            new_money = {'$set': {'money': new_money}}
            current_money = {'money': current_money}
            mymoney.update_one(current_money, new_money)
            custom_flash('You have successfully added an expense to your account.')
    return redirect(url_for('home'))

@app.route('/expenses/delete/<objectid>')
def delete_expense(objectid):
    if(check_if_session_is_active()):
        expense = {'_id': ObjectId(objectid)}
        myexpenses.delete_one(expense)
        custom_flash('You have successfully deleted that expense.')
    return redirect(url_for('home'))

@app.route('/income')
def income():
    if(check_if_session_is_active()):
        currentMoney = '{0:.2f}'.format(mymoney.find_one()['money'])
        incomes = myincome.find().sort('timestamp', -1)
        return render_template(
            'income.html',
            currentMoney=currentMoney,
            incomes=incomes,
            commarize=commarize
        )
    return render_template('landing.html')

@app.route('/money/add', methods=['POST', 'GET'])
def add_money():
    if(request.method == 'POST'):
        url = ('home', 'income')[request.form['from'] == 'income']
        if(check_if_session_is_active()):
            money = request.form['money']
            myincome.insert_one({
                'title': request.form['title'],
                'amount': money,
                'timestamp': f'{current_time()}'
            })
            x = mymoney.find_one()
            current_money = x['money']
            new_money = current_money + float(money)
            new_money = {'$set': {'money': new_money}}
            current_money = {'money': current_money}
            mymoney.update_one(current_money, new_money)
            custom_flash('You have successfully added money to your account.')
    return redirect(url_for(url))

@app.route('/income/delete/<objectid>')
def delete_income(objectid):
    if(check_if_session_is_active()):
        income = {'_id': ObjectId(objectid)}
        myincome.delete_one(income)
        custom_flash('You have successfully deleted that income.')
    return redirect(url_for('income'))

@app.route('/goals')
def goals():
    if(check_if_session_is_active()):
        incomes = myincome.find().sort('timestamp', -1)
        return render_template(
            'goals.html',
            incomes=incomes,
            commarize=commarize,
            currentMoney=mymoney.find_one()['money']
        )
    return redirect(url_for('home'))

##
# UTILITIES
##

def check_if_session_is_active():
    if('active' in session):
        return True
    return False

def custom_flash(message):
    flash(message)

def current_time():
    return datetime.datetime.now()

def commarize(number):
    return '{:,.2f}'.format(float(number))

if(__name__ == '__main__'):
    app.run('0.0.0.0', debug=True)
